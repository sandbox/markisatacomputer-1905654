<?php
function salesforce_inspect_object_options($sfapi) {
  $objects = cache_get('salesforce_inspect_object_options');
  if ($objects === FALSE) {
    $objects = array('' => '');
    foreach ($sfapi->objects() as $object) {
      $objects[$object['name']] = $object['label'];
    }
    cache_set('salesforce_inspect_object_options', $objects);
  } else {
    $objects = $objects->data;
  }
  
  return $objects;
}

/**
 * Return a form for a salesforce mapping entity.
 *
 * @param $form
 * @param $form_state
 * @param null $mapping
 *
 * @return mixed
 */
function salesforce_inspect_form($form, &$form_state) {
  $sfapi = salesforce_get_api();
  
  $default = !empty($form_state['values']['sfobject']) ? $form_state['values']['sfobject'] : FALSE;
  $options = salesforce_inspect_object_options($sfapi);

  $form['#attributes'] = array('id' => 'salesforce-inspect-form');
  $form['sfobject'] = array(
    '#title' => t('Salesforce Object'),
    '#type' => 'select',
    '#description' => t('Select a Salesforce Object.'),
    '#options' => $options,
    '#default_value' => $default,
    '#ajax' => array(
      'callback' => 'salesforce_inspect_form_callback',
      'wrapper' => 'salesforce-inspect-form',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['sfobject_results'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div class="clear-block" id="sfobject-results">',
    '#suffix' => '</div>',
  );

  if ($default) {
    $object = $sfapi->objectDescribe($default);
    if ($fields = $object['fields']) {
      $form['sfobject_results']['fields'] = array(
        '#theme' => 'salesforce_inspect_form_table',
        '#tree' => TRUE,
        '#attributes' => array('id' => 'salesforce_inspect_fields')
      );
      foreach ($fields as $delta => $field) {
        $form['sfobject_results']['fields'][$delta] = array('#value' => $field);
      }
    }
    if ( function_exists("kprint_r") ) {
      try {
        $dpm = kprint_r($object, true);
      } catch (Exception $e) {
        echo $e->faultstring;
      }
      if (strlen($dpm) < 1000000) { 
        $form['sfobject_results']['kprint'] = array(
          '#tree' => TRUE,
          '#markup' => $dpm
        );
      }
    }
  }

  return $form;
}

/**
 * Ajax callback for salesforce_mapping_form().
 */
function salesforce_inspect_form_callback($form, $form_state) {
  return $form;
}

/**
 * Validate salesforce_mapping_form().
 *
 * @param $form
 * @param $form_state
 */
function salesforce_inspect_form_validate($form, &$form_state) {
  
}

/**
 * Submit handler for salesforce_mapping_form().
 *
 * @param $form
 * @param $form_state
 */
function salesforce_inspect_form_submit($form, &$form_state) {
  
}
